from PIL import Image
from PIL.ExifTags import TAGS
import piexif
from ecdsa import SigningKey, NIST384p, VerifyingKey
from hashlib import sha256

def write_meta(file, sk, vk):
    image = Image.open(file)
    pix = image.load()
    pixraw = ''
    for x in range(image.size[0]):
        for y in range(image.size[1]):
            s = ''
            for j in pix[x,y]:
                s += str(j)
            pixraw+=s
    # Подпись
    s = str.encode(pixraw)
    M = sha256(s).digest()
    #print(M)
    signature = sk.sign(M)
    #print('signature:',signature)
    #print('result:',vk.verify(signature, M))
    
    zeroth_ifd = {piexif.ImageIFD.ImageDescription: vk.to_string().hex()+' '+signature.hex()}
    first_ifd = {piexif.ImageIFD.ImageDescription: vk.to_string().hex()+' '+signature.hex()}
    exif_dict = {"0th":zeroth_ifd, "1st":first_ifd}
    exif_bytes = piexif.dump(exif_dict)
    im = Image.open("image.jpg")
    im.save("out.jpg", exif=exif_bytes)


def cheek_meta(file):
    image = Image.open(file)
    exifdata = image.getexif()
    for tag_id in exifdata:
        # получить имя тега вместо идентификатора
        tag = TAGS.get(tag_id, tag_id)
        data = exifdata.get(tag_id)
        # декодировать байты 
        if isinstance(data, bytes):
            data = data.decode()
    str_list = data.split(sep=' ')
    vk_string = str_list[0]
    vk_new = VerifyingKey.from_string(vk_string, curve=NIST384p)
    signature_new = str_list[1]
    pix = image.load()
    pixraw = ''
    for x in range(image.size[0]):
        for y in range(image.size[1]):
            s = ''
            for j in pix[x,y]:
                s += str(j)
            pixraw+=s
    # Подпись
    s = str.encode(pixraw)
    M = sha256(s).digest()
    result = vk_new.verify(signature_new, M)
    return result