from PIL import Image
from PIL.ExifTags import TAGS
import piexif
from ecdsa import SigningKey, NIST384p
from hashlib import sha256

#open file 
file = "image.jpg"
image = Image.open(file)

# read meta
exifdata = image.getexif()
for tag_id in exifdata:
    # получить имя тега вместо идентификатора
    tag = TAGS.get(tag_id, tag_id)
    data = exifdata.get(tag_id)
    # декодировать байты 
    if isinstance(data, bytes):
        data = data.decode()
    print(f"{tag:25}: {data}")

#строка пикселей
pix = image.load()
pixraw = ''
for x in range(image.size[0]):
    for y in range(image.size[1]):
        s = ''
        for j in pix[x,y]:
            s += str(j)
        pixraw+=s

# Генерация ЗК
sk = SigningKey.generate(curve=NIST384p)
print('sk:', sk.to_string())
# Генерация ОК
vk = sk.verifying_key
print('vk:', vk.to_string().hex())
# Подпись
s = str.encode(pixraw)
M = sha256(s).digest()
print(M)
signature = sk.sign(M)
print('signature:',signature)
print('result:',vk.verify(signature, M))

#запись подписи в метаданные
zeroth_ifd = {piexif.ImageIFD.ImageDescription: vk.to_string().hex()+' '+signature.hex()}
first_ifd = {piexif.ImageIFD.ImageDescription: vk.to_string().hex()+' '+signature.hex()}
exif_dict = {"0th":zeroth_ifd, "1st":first_ifd}
exif_bytes = piexif.dump(exif_dict)
im = Image.open("image.jpg")
im.save("out.jpg", exif=exif_bytes)

#чтение подписи в новых метаданных
file = "out.jpg"
image = Image.open(file)

exifdata = image.getexif()
for tag_id in exifdata:
    # получить имя тега вместо идентификатора
    tag = TAGS.get(tag_id, tag_id)
    data = exifdata.get(tag_id)
    # декодировать байты 
    if isinstance(data, bytes):
        data = data.decode()
    print(f"{tag:25}: {data}")