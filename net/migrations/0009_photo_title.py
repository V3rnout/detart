# Generated by Django 4.1.7 on 2023-03-19 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('net', '0008_alter_photo_pic'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='title',
            field=models.CharField(default='test', max_length=100),
        ),
    ]
