from PIL import Image
import cv2
from cairosvg import svg2png
import os
from moviepy.editor import *


def water_mark(input_path, watermark_path, output_path):
    # Открываем изображение и водяной знак
    save_path = output_path
    if input_path.endswith('.mp4') or input_path.endswith('.mkv') or input_path.endswith('.gif'):
        # Открываем видео и получаем информацию о нем
        video = cv2.VideoCapture(input_path)
        fps = video.get(cv2.CAP_PROP_FPS)
        frame_size = (int(video.get(cv2.CAP_PROP_FRAME_WIDTH)), int(video.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        # Создаем объект для записи видео в нужном формате
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        writer = cv2.VideoWriter(output_path, fourcc, fps, frame_size)

        # Загружаем водяной знак
        watermark_image = cv2.imread(watermark_path, cv2.IMREAD_UNCHANGED)
        watermark_height, watermark_width, _ = watermark_image.shape
        watermark_ratio = min(frame_size[0] / watermark_width, frame_size[1] / watermark_height) * 0.5
        new_width = int(watermark_width * watermark_ratio * 0.5)
        new_height = int(watermark_height * watermark_ratio * 0.5)
        watermark_image = cv2.resize(watermark_image, (new_width, new_height), interpolation=cv2.INTER_LANCZOS4)

        while True:
            # Считываем кадр из видео
            success, frame = video.read()
            if not success:
                break

            # Накладываем водяной знак на кадр
            watermark_position = (frame_size[0] - new_width, 0)
            alpha_s = watermark_image[:, :, 3] / 255.0
            alpha_l = 1.0 - alpha_s
            for c in range(0, 3):
                frame[0:new_height, watermark_position[0]:frame_size[0], c] = (alpha_s * watermark_image[:, :, c] + alpha_l * frame[0:new_height, watermark_position[0]:frame_size[0], c])

            # Записываем кадр в выходной файл
            writer.write(frame)

        # Освобождаем ресурсы
        video.release()
        writer.release()
        cv2.destroyAllWindows()

        #print(f"Watermarked video saved to '{output_path}'")

    elif input_path.endswith('.jpg') or input_path.endswith('.jpeg') or input_path.endswith('.png') or input_path.endswith('.webp') or input_path.endswith('.svg') or input_path.endswith('.bmp'):
        # Определяем расширение файла
        file_ext = os.path.splitext(input_path)[1]

        
        # Открываем изображение и получаем информацию о нем
        with Image.open(input_path) as image:
            # Проверяем, имеет ли изображение альфа-канал
            if image.mode == "RGBA":
                # Конвертируем изображение в RGB формат
                image = image.convert("RGB")
            if image.format == "PNG" and "transparency" in image.info:
                transparent_color = image.info["transparency"]
            else:
                transparent_color = None

            # Загружаем водяной знак
            with Image.open(watermark_path) as watermark_image:
        # Проверяем, имеет ли изображение альфа-канал
                if image.mode == "RGBA":
            # Конвертируем изображение в RGB формат
                    image = image.convert("RGB")
                if image.format == "PNG" and "transparency" in image.info:
                    transparent_color = image.info["transparency"]
                else:
                    transparent_color = None

                with Image.open(watermark_path) as watermark_image:

                    # Получаем размеры изображений
                    image_width, image_height = image.size
                    watermark_width, watermark_height = watermark_image.size

                    with Image.open(watermark_path) as watermark_image:
                            # Уменьшаем размер водяного знака в соответствии с размером исходного изображения
                        watermark_ratio = min(image_width / watermark_width, image_height / watermark_height) * 0.5
                        new_width = int(watermark_width * watermark_ratio * 0.5)
                        new_height = int(watermark_height * watermark_ratio * 0.5)
                        watermark_image = watermark_image.resize((new_width, new_height), resample=Image.LANCZOS)

                            # Позиционируем водяной знак в правом верхнем углу
                        watermark_position = (image_width - new_width, 0)
                        image.paste(watermark_image, watermark_position, watermark_image)


                    # Накладываем водяной знак на изображение
                    if transparent_color is not None:
                        # Если исходное изображение содержит прозрачный фон, сохраняем его в PNG-формате
                        image.alpha_composite(watermark_image, watermark_position)
                        #save_path = "C:/Users/Volnost/Desktop/output_image.png"
                        image.save(save_path, "PNG", quality=100, optimize=True, transparency=transparent_color, subsampling=0)
                    else:
                        # Иначе сохраняем изображение без прозрачности в JPEG-формате
                        image.paste(watermark_image, watermark_position, watermark_image)
                        #save_path = "C:/Users/Volnost/Desktop/output_image.jpeg"
                        image.convert("RGB").save(save_path, "JPEG", quality=80, optimize=True)

