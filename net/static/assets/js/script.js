const form = document.querySelector("form"),
    fileInput = document.querySelector(".file-input"),
    progressArea = document.querySelector(".progress-area"),
    uploadedArea = document.querySelector(".uploaded-area");

// form click event
form.addEventListener("click", () => {
    fileInput.click();
});

fileInput.onchange = ({ target }) => {
    let file = target.files[0]; //getting file [0] this means if user has selected multiple files then get first one only
    if (file) {
        let fileName = file.name; //getting file name
        // if (fileName.length >= 20) { //if file name length is greater than 12 then split it and add ...
        //     let splitName = fileName.split('.');
        //     fileName = splitName[0].substring(0, 21) + "... ." + splitName[1];
        // }
        uploadFile(fileName); //передача имени загружаемого файла
    }
}

// file upload function
function uploadFile(name) {
    let xhr = new XMLHttpRequest(); //создания xnr объекта
    xhr.open("POST", "/", true); //отправка пост-запроса
    xhr.addEventListener("load", transferComplete);
    xhr.upload.addEventListener("progress", ({ loaded, total }) => { //прогресс загрузки файла
        let fileLoaded = Math.floor((loaded / total) * 100); //прогресс загрузки файла в процентах
        let fileTotal = Math.floor(total / 1000); //размер файла в Кб
        let fileSize;
        // if file size is less than 1024 then add only KB else convert this KB into MB
        (fileTotal < 1024) ? fileSize = fileTotal + " KB": fileSize = (loaded / (1024 * 1024)).toFixed(2) + " MB";
        let progressHTML = `<li class="info">
                          <i class="fas fa-file-alt"></i>
                          <div class="zagruzka">
                            <div class="details">
                              <span class="name">${name} • Загрузка</span>
                              <span class="percent">${fileLoaded}%</span>
                            </div>
                            <div class="progress-bar">
                              <div class="progress" style="width: ${fileLoaded}%"></div>
                            </div>
                          </div>
                        </li>`;
        uploadedArea.innerHTML = ""; //uncomment this line if you don't want to show upload history
        uploadedArea.classList.add("onprogress");
        progressArea.innerHTML = progressHTML;
        if (loaded == total) {
            progressArea.innerHTML = "";
            let uploadedHTML = `<li class="info">
                            <div class="content_upload">
                              <i class="name fa-file-alt"></i>
                              <div class="details">
                                <span class="name"> ${name}  •  Загружен   </span> <span class="size">${fileSize}</span>
                              </div>
                            </div>
                            <i class="fas fa-check"></i>
                          </li>`;
            uploadedArea.classList.remove("onprogress");
            uploadedArea.innerHTML = uploadedHTML; //uncomment this line if you don't want to show upload history
            $(function() {
                var newElems= $("<div class='scan' id='2'/>")
                .append("<div class='face'/>")
                .append("<dov class='dots'/>")
                .append('<h3>Проверка подлинности</h3>')
            $('#1').children().first().replaceWith(newElems);
            });

            // uploadedArea.insertAdjacentHTML("afterbegin", uploadedHTML); //remove this line if you don't want to show upload history
        }
    });
    let data = new FormData(form); //объект для отравки данных из формы
    xhr.send(data); //Отправка данных из формы
}

function transferComplete(){
  window.location.href =  "truly/";
}



// $(function() {
//   $('.start').click(function() {
//     var newElems= $("<div class='scan' id='2'/>")
//     .append("<div class='face'/>")
//     .append("<dov class='dots'/>")
//     .append('<h3>Проверка подлинности</h3>')
// $('#1').children().first().replaceWith(newElems);
// $(this).prop('disabled',true) })});
         
//   });
// });
// $(function(){
// $('.modal_close').click(function() {
//   var oldElems= $("div class='container' id='1'/>")
//   .append("<div class='upload'/>")
//   .append("<label class='lets'>Нажмите на область для выбора файла</label>")
//   .append("<form action='#'/>")
//   .append("<input class='file-input' type='file' name='file' hidden />")
//   .append("<div class='poddon'/>")
//   .append("div class='knopka'/>")
//   .append("button class='start'>Запуск</button>")
//   .append("<div class='info'/>")
//   .append("section class='progress-area/>")
//   .append("section class='uploaded-area/>")
// $('#2').children().first().replaceWith(oldElems);
// });
// });


// if (CONDITION) {
//   alert('{{ message }}');
// }
 
// setTimeout(function(){
//   window.location.href = '#';
// }, 10 * 1000);

